import { kebabCase } from '../../../utils';

describe('[utils]: font-cases', () => {
  test('kebabCase: success', async () => {
    const result = kebabCase('Test-Test_test test');

    expect(result).toEqual('test-test-test-test');
  });
});
