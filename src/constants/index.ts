export const EXTS = {
  SVG: 'svg',
  TSX: 'tsx',
  TS: 'ts',
  CSS: 'css',
  JSON: 'json',
} as const;
export const ENCODING = { UTF8: 'utf-8' } as const;

export const RGX = {
  META_TAG: /\*@\*/g,
  ALL: /([\s\S]*?)/g,
  ALL_TO_LINE: /(.*)/g,
  METADATA: /\*@\*([\s\S]*?)\*@\*/g,
} as const;

export const PARSE_TYPES = {
  INSERT_TO_END: 'insert-to-end',
  INSERT_IMPORT_TO_END: 'insert-import-to-end',
} as const;

export const FILE_NAME_CONFIG = 'create-template' as const;

export const ROOT = process.cwd();

export const REQUIRED_FIELDS_FOR_CONFIG = {
  PATH_TO_TEMPLATES: 'path-to-templates',
  TEMPLATES: 'templates',
};
