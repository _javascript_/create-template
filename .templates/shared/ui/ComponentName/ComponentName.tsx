import React from "react";
import { clsx } from "shared/lib/clsx";

import { {{ComponentName}}Provider } from "./components";
import { use{{ComponentName}} } from "./hooks";

import { {{ComponentName}}_VARIANTS } from "./config";
import { bem } from "./utils";

namespace {{ComponentName}} {
  type {{ComponentName}}Variants = typeof {{ComponentName}}_VARIANTS;
  export type Variant = {{ComponentName}}Variants[keyof {{ComponentName}}Variants];
  export type Props = {
    className?: string;
    variant?: Variant;
    children?: React.ReactNode;
  };
}

function {{ComponentName}}Component(props: {{ComponentName}}.Props) {
  const { className, children, variant = ""} = props;

  const classesRoot = clsx(bem(), className);

  return (
    <div className={clsx(classesRoot)}>
      {children}
    </div>
  );
}

const {{ComponentName}} = React.memo({{ComponentName}}Component);

export { {{ComponentName}} };
