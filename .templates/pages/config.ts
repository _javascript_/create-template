*@*
{
    "type": "insert-import-to-end",
    "content": "import { {{PAGE_NAME}} } from \"./{{page-name}}\";",
    "defaultContent": [
        "import { getRouteConfigs } from \"shared/lib/router\";"
    ]
}
*@*

*@*
{
    "type": "insert-to-end",
    "start": "const pages = [",
    "content": "{{PAGE_NAME}}",
    "end": "];"
}
*@*

export const ROUTE_CONFIGS = getRouteConfigs(pages);
