import fs from 'fs';
import path from 'path';

function splitpIntoFileAndFolders(p: string) {
  const parse = path.parse(p);
  const dirs = parse.dir.split(path.sep);
  const file = parse.base;
  return { dirs, file };
}

export function createFileRecursive(p: string, data = '') {
  if (!fs.existsSync(p)) {
    const { dirs } = splitpIntoFileAndFolders(p);
    dirs.reduce((acc, dir) => {
      acc = path.join(acc, dir);
      if (!fs.existsSync(acc)) fs.mkdirSync(acc);
      return acc;
    }, '');

    fs.writeFileSync(p, data, 'utf8');
  }
}

export function readFileOrCreate(p: string, data = '') {
  if (!fs.existsSync(p)) {
    const { dirs } = splitpIntoFileAndFolders(p);
    dirs.reduce((acc, dir) => {
      acc = path.join(acc, dir);
      fs.mkdirSync(acc);
      return acc;
    }, '');
    fs.writeFileSync(p, data, 'utf8');
  }

  return fs.readFileSync(p, 'utf8');
}

export function readDirOrCreate(p: string) {
  if (!fs.existsSync(p)) {
    fs.mkdirSync(p);
  }
  return fs.readdirSync(p);
}
