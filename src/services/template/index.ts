import fs from 'fs';
import path from 'path';
import { ENCODING, RGX } from '../../constants';
import {
  createFileRecursive,
  getAllFontCases,
  getFontCaseFromString,
} from '../../utils';

import {
  Answers,
  TemplateMetaData,
  InsertToEnd,
  InsertImportToEnd,
  InsertExportToEnd,
  Assets,
  UtilityToAnswer,
} from '../../types';
import { Config } from '../config';
import { Questions } from '../questions';

type TemplateOptions = {
  config: Config;
  questions: Questions;
  assets: Assets;
};

export class Template {
  private config: Config;
  private questions: Questions;
  private assets: Assets;

  private constructor(options: TemplateOptions) {
    const { assets, config, questions } = options;
    this.assets = assets;
    this.config = config;
    this.questions = questions;
  }

  public static async init(config: Config, questions: Questions) {
    const assets = Template.getDataFromAnswers(questions.answers);
    return new Template({ config, questions, assets });
  }

  private static getDataFromAnswers(answers: Answers): Assets {
    const utilitiesToAnswer: UtilityToAnswer[] = [];
    const templateOptions: string[] = [];

    for (const answer of Object.entries(answers)) {
      const [key, value] = answer;
      if (typeof value === 'boolean') {
        value && templateOptions.push(key);
        continue;
      }
      if (typeof value === 'object') {
        value.forEach((item) => templateOptions.push(item));
        continue;
      }
      if (value === 'add-path') {
        console.log({ value });
        continue;
      }

      const fontCasesKey = getAllFontCases(key);
      const fontCasesReplacement = getAllFontCases(value);
      const rgxSource = Object.values(fontCasesKey).join('|');
      const rgxFindValueFromTarget = new RegExp(rgxSource, 'g');
      const rgxFindTarget = new RegExp(`{{(${rgxSource})}}`, 'g');

      const item = {
        rgxFindValueFromTarget,
        rgxFindTarget,
        fontCasesReplacement,
        fontCasesKey,
      };

      utilitiesToAnswer.push(item);
    }

    return { utilitiesToAnswer, templateOptions };
  }

  public start() {
    const dirs = this.config.pathsToTemplates[this.questions.templateName];
    const { pathToDirFromTemplates } = this.config;
    for (const dir of dirs) {
      const pathToTemplate = path.join(pathToDirFromTemplates, dir);
      this.readDir(pathToTemplate);
    }
  }

  private readDir(dirPath: string) {
    const dir = fs.readdirSync(dirPath);
    const { dirs, files } = this.getFilesAndDirs(dir);
    for (const dir of dirs) {
      const nextDirPath = path.join(dirPath, dir);
      this.readDir(nextDirPath);
    }
    this.fileAnalysis(dirPath, files);
  }

  private fileAnalysis(pathDir: string, files: string[]) {
    const { pathToResult, pathToDirFromTemplates } = this.config;

    for (const file of files) {
      const filePath = path.join(pathDir, file);

      const data = fs.readFileSync(filePath, ENCODING.UTF8);

      const pathTargetFile = filePath.replace(
        pathToDirFromTemplates,
        pathToResult,
      );

      const pathToCreateFile = this.getPathToCreateFile(pathTargetFile);

      let newData = this.prepareFileData(data);

      if (newData.match(RGX.META_TAG)) {
        const result = this.parseSettingTag(pathToCreateFile, newData);
        if (result.dontCreateFile) continue;
        newData = result.newData;
      }

      if (fs.existsSync(pathToCreateFile)) {
        fs.unlinkSync(pathToCreateFile);
      }
      createFileRecursive(pathToCreateFile, this.prettierData(newData));
    }
  }

  private prettierData(data: string) {
    const lines = data.split('\n');

    let isVerstLineEmpty = true;
    let emptyLineCounter = 0;
    const newLines = lines.filter((line) => {
      if (!line.trim()) {
        if (isVerstLineEmpty) return false;
        ++emptyLineCounter;
        if (emptyLineCounter > 1) return false;
      }
      isVerstLineEmpty = false;
      emptyLineCounter = 0;
      return true;
    });

    return [...newLines, ''].join('\n');
  }

  private getPathToCreateFile(path: string) {
    for (const item of this.assets.utilitiesToAnswer) {
      const { fontCasesReplacement, rgxFindValueFromTarget } = item;

      path = path.replace(rgxFindValueFromTarget, (targer) => {
        const fontCase = getFontCaseFromString(targer);
        return fontCasesReplacement[fontCase];
      });
    }

    return path;
  }

  private parseSettingTag(targetFilePath: string, data: string) {
    let dontCreateFile = false;

    const newData = data.replace(RGX.METADATA, (target) => {
      if (dontCreateFile) return '';
      const value = target.replace(RGX.META_TAG, '');
      const metadata = JSON.parse(value) as TemplateMetaData;
      const result = this.handlerMetadata(targetFilePath, metadata);
      if (typeof result === 'boolean') {
        if (!result) dontCreateFile = true;
        return '';
      }
      return result;
    });

    return { newData, dontCreateFile };
  }

  private handlerMetadata(targetFilePath: string, metadata: TemplateMetaData) {
    const { type } = metadata;

    // ------
    // Другие обработчики if (type === 'bla-bla-bla')
    // ------

    if (type === 'create-file-if') {
      const { value } = metadata;
      const is = this.assets.templateOptions.includes(value);
      return is;
    }

    if (type === 'create-block-if') {
      const { content, value } = metadata;
      const is = this.assets.templateOptions.includes(value);
      const c = typeof content === 'string' ? content : content.join('\n');
      if (is) return c;
      return '';
    }

    if (!fs.existsSync(targetFilePath)) {
      if (type === 'insert-import-to-end') {
        const { content, defaultContent = [] } = metadata;
        return `${defaultContent.join('\n')}\n${content}`;
      }
      if (type === 'insert-export-to-end') {
        const { content } = metadata;
        return `${content}`;
      }
      if (type === 'insert-to-end') {
        const { content, end, start } = metadata;
        return `${start}\n  ${content},\n${end}`;
      }
      return '';
    }

    const currentData = fs.readFileSync(targetFilePath, ENCODING.UTF8);

    if (type === 'insert-to-end') {
      return this.handlerMetadataInsertToEnd(currentData, metadata);
    }

    if (type === 'insert-export-to-end') {
      return this.handlerMetadataInsertExportToEnd(currentData, metadata);
    }

    if (type === 'insert-import-to-end') {
      return this.handlerMetadataInsertImportToEnd(currentData, metadata);
    }

    return '';
  }

  private handlerMetadataInsertToEnd(data: string, metadata: InsertToEnd) {
    const lines = data.split('\n');
    const { start, end, content } = metadata;

    const index = lines.findIndex((line) => line.includes(start));

    if (index < 0) return `${start}\n${content},\n${end}`;

    const endLines = lines.slice(index);
    const endIndex = endLines.findIndex((line) => line.includes(end));

    const newLine = [];
    for (let index = 0; index < endLines.length; index++) {
      const line = endLines[index];
      if (index !== endIndex) {
        newLine.push(line);
        continue;
      }
      const replacement = line.replace(end, () => {
        return `  ${content},\n${end}`;
      });
      newLine.push(replacement);
      break;
    }

    const result = newLine.join('\n');

    return result;
  }

  private handlerMetadataInsertImportToEnd(
    data: string,
    metadata: InsertImportToEnd,
  ) {
    const { content, defaultContent = [] } = metadata;
    const lines = data
      .split('\n')
      .filter((line) => !defaultContent.includes(line));

    const index = [...lines]
      .reverse()
      .findIndex((line) => line.includes('import'));

    const lastIndex = lines.length - index;

    const importsLines = lines.slice(0, lastIndex);

    const newLines = [...defaultContent, '\r', ...importsLines, `${content}\r`];
    const result = newLines.join('\n');

    return result;
  }

  private handlerMetadataInsertExportToEnd(
    data: string,
    metadata: InsertExportToEnd,
  ) {
    const { content } = metadata;
    const lines = data.split('\n').filter((line) => !line.includes(content));
    const newLines = [...lines, `${content}`];
    const result = newLines.join('\n');
    return result;
  }

  private prepareFileData(data: string) {
    let newData = data;
    for (const item of this.assets.utilitiesToAnswer) {
      const { rgxFindTarget, rgxFindValueFromTarget, fontCasesReplacement } =
        item;

      newData = newData.replace(rgxFindTarget, (target) => {
        const value = target.match(rgxFindValueFromTarget)?.[0];
        const targetCase = getFontCaseFromString(value);
        return fontCasesReplacement[targetCase];
      });
    }
    return newData;
  }

  private getFilesAndDirs(files: string[]) {
    return files.reduce<{ files: string[]; dirs: string[] }>(
      (acc, file) => {
        const [, ext] = file.split('.');
        if (ext) acc.files.push(file);
        else acc.dirs.push(file);
        return acc;
      },
      { files: [], dirs: [] },
    );
  }
}
