import React from "react";

export type {{ComponentName}}ContextValue = {};

export const {{ComponentName}}Context = React.createContext({} as {{ComponentName}}ContextValue);
