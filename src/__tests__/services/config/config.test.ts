import { Config } from '../../../services';

describe('[Services]: config', () => {
  test('Singleton', async () => {
    const config1 = await Config.getInstance();
    const config2 = await Config.getInstance();

    expect(config1).toBe(config2);
  });
});
