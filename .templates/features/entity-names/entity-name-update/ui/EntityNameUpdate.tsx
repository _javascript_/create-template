import React from 'react';

import { clsx } from 'shared/lib/clsx';

import { bem } from '../utils';

import './{{EntityName}}Update.css';


namespace {{EntityName}}Update {
  export type Props = {
    className: string;
    children: React.ReactNode;
  }
}

function {{EntityName}}Update(props: {{EntityName}}Update.Props) {
  const { className, children } = props;
  const classes = clsx(bem(), className);

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

export { {{EntityName}}Update };
