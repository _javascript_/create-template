export * as {{entityName}}DetailsConfig from './config';
export * as {{entityName}}DetailsService from './service';
export * as UI{{EntityName}}Details from './ui';
export * as {{entityName}}DetailsUtils  from './utils';
export * as {{entityName}}DetailsTypes  from './types';
