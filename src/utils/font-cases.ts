function ucFirst(str = '') {
  if (!str) return str;
  return str[0].toUpperCase() + str.slice(1);
}

function ulFirst(str = '') {
  if (!str) return str;
  return str[0].toLowerCase() + str.slice(1);
}

function splitWords(string = '') {
  return string.replace(/([a-z])([A-Z])/g, '$1-$2').replace(/[\s_]+/g, '-');
}

// camelCase
export function camelCase(string = '') {
  return splitWords(string)
    .split('-')
    .map((word, index) => {
      return index ? ucFirst(word) : ulFirst(word);
    })
    .join('');
}
// snake_case
export function snakeCase(string = '') {
  return splitWords(string)
    .split('-')
    .map((word) => {
      return ulFirst(word);
    })
    .join('_');
}
// kebab-case
export function kebabCase(string = '') {
  return splitWords(string)
    .split('-')
    .map((word) => {
      return ulFirst(word);
    })
    .join('-');
}

// PascalCase
export function pascalCase(string = '') {
  return splitWords(string)
    .split('-')
    .map((word) => {
      return ucFirst(word);
    })
    .join('');
}
// UPPER_CASE_SNAKE_CASE
export function upperCase(string = '') {
  return splitWords(string)
    .split('-')
    .map((word) => {
      return word.toUpperCase();
    })
    .join('_');
}

export function getAllFontCases(string = '') {
  return {
    upper: upperCase(string),
    pascal: pascalCase(string),
    kebab: kebabCase(string),
    snake: snakeCase(string),
    camel: camelCase(string),
  };
}

export function getFontCaseFromString(string = '') {
  const cases = {
    upper: upperCase(string),
    pascal: pascalCase(string),
    kebab: kebabCase(string),
    snake: snakeCase(string),
    camel: camelCase(string),
  } as const;
  const value = Object.entries(cases).find(([, value]) => value === string);
  const result = value?.[0] ?? 'kebab';
  return result as keyof ReturnType<typeof getAllFontCases>;
}
