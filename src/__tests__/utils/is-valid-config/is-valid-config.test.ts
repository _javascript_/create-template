import { REQUIRED_FIELDS_FOR_CONFIG } from '../../../constants';
import { isValidConfig } from '../../../utils';

describe('[utils]: parse-config', () => {
  test('Pending required fields: success', async () => {
    const expected = Object.fromEntries(
      Object.entries(REQUIRED_FIELDS_FOR_CONFIG).map(([, value]) => [
        value,
        '',
      ]),
    );
    const result = await isValidConfig(expected);

    expect(result).toEqual(true);
  });

  test('Pending required fields: fail', async () => {
    const result = await isValidConfig({});

    expect(result).toEqual(false);
  });
});
