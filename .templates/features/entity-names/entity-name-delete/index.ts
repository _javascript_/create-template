export * as {{entityName}}DeleteConfig from './config';
export * as {{entityName}}DeleteService from './service';
export * as UI{{EntityName}}Delete from './ui';
export * as {{entityName}}DeleteUtils  from './utils';
export * as {{entityName}}DeleteTypes  from './types';
