import path from 'path';
import { EXTS } from '../../../constants';
import { parseConfig } from '../../../utils';

const MOCK_FILE_PATHS = {
  PARSE_FILE: 'parse-file',
};

const EXPECTED_DATA = {
  PARSE_FILE: { test: 'test' },
};

describe('[utils]: parse-config', () => {
  test('Parse json file', async () => {
    const pathToConfig = path.join(
      __dirname,
      `${MOCK_FILE_PATHS.PARSE_FILE}.${EXTS.JSON}`,
    );
    const expected = EXPECTED_DATA.PARSE_FILE;
    const result = await parseConfig(pathToConfig);
    expect(result).toEqual(expected);
  });
});
