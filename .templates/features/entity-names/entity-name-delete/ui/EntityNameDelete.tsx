import React from 'react';

import { clsx } from 'shared/lib/clsx';

import { bem } from '../utils';

import './{{EntityName}}Delete.css';


namespace {{EntityName}}Delete {
  export type Props = {
    className: string;
    children: React.ReactNode;
  }
}

function {{EntityName}}Delete(props: {{EntityName}}Delete.Props) {
  const { className, children } = props;
  const classes = clsx(bem(), className);

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

export { {{EntityName}}Delete };
