import { ACCESS_LEVELS } from "../access";
import { LAYOUT_NAMES } from "../layouts";

import { PAGE_NAMES } from "./_page-names";
import { PageMetaData } from "./_types";

export const pageName = PAGE_NAMES.{{PAGE_NAME}};

export const path = () => `/{{page-path}}`;

export const metadata: PageMetaData = {
  pageName,
  layout: {
    [ACCESS_LEVELS.GUEST]: LAYOUT_NAMES.HEADER_FOOTER,
  },
  access: {
    allowed: [ACCESS_LEVELS.GUEST],
    redirect: errorPath("403"),
  },
  path: path(),
};

const redirectFrom = "{{page-path}}";
const redirectTo = path("{{page-path}}");

export const redirects = {};
