import { REQUIRED_FIELDS_FOR_CONFIG } from '../constants';

export async function isValidConfig(config: object) {
  const requiredFields = Object.values(REQUIRED_FIELDS_FOR_CONFIG);
  const result = Object.keys(config).filter((field) => {
    return requiredFields.some((item) => item === field);
  });
  return result.length === requiredFields.length;
}
