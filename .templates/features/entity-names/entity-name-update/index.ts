export * as {{entityName}}UpdateConfig from './config';
export * as {{entityName}}UpdateService from './service';
export * as UI{{EntityName}}Update from './ui';
export * as {{entityName}}UpdateUtils  from './utils';
export * as {{entityName}}UpdateTypes  from './types';
