export * as {{entityName}}Config from './config';
export * as {{entityName}}sService from './service';
export * as UI{{EntityName}} from './ui';
export * as {{entityName}}Utils  from './utils';
export * as {{entityName}}Types  from './types';