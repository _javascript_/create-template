import { getAllFontCases } from '../utils';

export type WelcomeOptions = {
  title?: string;
  description?: string;
};
export type Question = {
  key: string;
  type: string;
  message: string;
  choices: { name: string; value: string }[];
};

export type AllQuestions = {
  [template: string]: Question[];
};

export type Answers = { [question: string]: boolean | string | string[] };

export type UtilityToAnswer = {
  rgxFindTarget: RegExp;
  rgxFindValueFromTarget: RegExp;
  fontCasesReplacement: ReturnType<typeof getAllFontCases>;
  fontCasesKey: ReturnType<typeof getAllFontCases>;
};

export type Assets = {
  utilitiesToAnswer: UtilityToAnswer[];
  templateOptions: string[];
};

export type InsertToEnd = {
  type: 'insert-to-end';
  start: string;
  content: string;
  end: string;
};

export type InsertImportToEnd = {
  type: 'insert-import-to-end';
  content: string;
  defaultContent?: string[];
};

export type InsertExportToEnd = {
  type: 'insert-export-to-end';
  content: string;
};

export type CreateFileIf = {
  type: 'create-file-if';
  value: string;
};

export type CreateBlockIf = {
  type: 'create-block-if';
  value: string;
  content: string | string[];
};

export type TemplateMetaData =
  | InsertToEnd
  | InsertImportToEnd
  | CreateBlockIf
  | CreateFileIf
  | InsertExportToEnd;
