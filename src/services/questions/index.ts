import inquirer from 'inquirer';

import { Answers, AllQuestions, Question } from '../../types';
import { Config } from '../config';

export class Questions {
  public answers: Answers;
  public templateName: string;

  private constructor(dto: { answers: Answers; templateName: string }) {
    this.answers = dto.answers;
    this.templateName = dto.templateName;
  }

  static async init(config: Config) {
    const { questions } = config;
    const templateName = await Questions.askTemplate(questions);
    const targetQuestions = questions[templateName];
    const answers = await Questions.askTemplateQuestions(targetQuestions);
    return new Questions({ answers, templateName });
  }

  private static async askTemplate(questions: AllQuestions) {
    const choices = Object.keys(questions);
    const answers = await inquirer.prompt({
      name: 'template',
      type: 'list',
      message: 'Какой шаблон использовать?',
      choices: choices,
    });
    return answers.template;
  }

  private static async askTemplateQuestions(questions: Question[]) {
    if (!questions || !questions.length) {
      throw new Error('[Service AskQuestions]: not found questions');
    }

    let answers = {};

    for await (const question of questions) {
      const answer: any = await inquirer.prompt({
        name: question.key,
        type: question.type as any,
        message: question.message,
        choices: question.choices,
      });
      answers = { ...answers, ...answer };
    }

    return answers;
  }
}
