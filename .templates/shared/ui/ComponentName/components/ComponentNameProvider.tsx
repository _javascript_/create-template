import React from 'react';

import { {{ComponentName}}Context, {{ComponentName}}ContextValue } from '../{{ComponentName}}Context'

export type {{ComponentName}}ProviderProps = {
  children: React.ReactNode;
  value: {{ComponentName}}ContextValue;
};

export function {{ComponentName}}Provider(props: {{ComponentName}}ProviderProps) {
  const { children, value } = props;
  return (
    <{{ComponentName}}Context.Provider value={value}>{children}</{{ComponentName}}Context.Provider>
  );
}
