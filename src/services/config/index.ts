import path from 'path';

import { EXTS, FILE_NAME_CONFIG, ROOT } from '../../constants';
import { AllQuestions } from '../../types';
import { parseConfig, isValidConfig } from '../../utils';

export class Config {
  private static instance: Config;

  public pathToDirFromTemplates = 'templates';
  public pathToResult = 'src';

  public questions: AllQuestions = {};
  public allQuestionsKeys: string[] = [];

  public pathsToTemplates: { [templateName: string]: string[] } = {};

  public welcome = undefined;

  private constructor(config: any) {
    this.pathToDirFromTemplates = path.join(
      // ROOT,
      config?.['path-to-templates'],
    );
    this.pathToResult = path.join(
      // ROOT,
      config?.['path-to-result'],
    );

    this.questions = this.getAllQuestions(config);
    this.pathsToTemplates = this.getPathsToTemplates(config);

    this.welcome = config?.welcome;

    this.getAllQuestionsKeys();
  }

  static async getInstance() {
    if (Config.instance) return Config.instance;

    const pathToConfig = path.join(ROOT, `${FILE_NAME_CONFIG}.${EXTS.JSON}`);

    const config = await parseConfig(pathToConfig);
    if (!isValidConfig(config)) {
      throw new Error(
        `[Service Config]: not valid file ${FILE_NAME_CONFIG}.${EXTS.JSON}`,
      );
    }

    Config.instance = new Config(config);
    return Config.instance;
  }

  private getAllQuestionsKeys() {
    const allQuestionsKeys = Object.entries(this.questions).reduce<string[]>(
      (acc, [, value]) => {
        const keys = value.map((item) => item.key);
        acc = [...acc, ...keys];
        return acc;
      },
      [],
    );

    this.allQuestionsKeys = allQuestionsKeys;
  }

  private getAllQuestions(config: any): AllQuestions {
    if (!config.templates)
      throw new Error(`[Service Config]: not found templates`);

    return Object.entries(config.templates).reduce<AllQuestions>(
      (acc, [key, value]: any) => {
        if (value.questions) acc[key] = value.questions;
        return acc;
      },
      {},
    );
  }

  private getPathsToTemplates(config: any) {
    if (!config.templates)
      throw new Error(`[Service Config]: not found templates`);

    return Object.entries(config.templates).reduce<any>(
      (acc, [key, value]: any) => {
        if (value.questions) acc[key] = value.paths;
        return acc;
      },
      {},
    );
  }
}
