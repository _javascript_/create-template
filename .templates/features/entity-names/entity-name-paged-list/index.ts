export * as {{entityName}}PagedListConfig from './config';
export * as {{entityName}}PagedListService from './service';
export * as UI{{EntityName}}PagedList from './ui';
export * as {{entityName}}PagedListUtils  from './utils';
export * as {{entityName}}PagedListTypes  from './types';
