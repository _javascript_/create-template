import { uiBem } from 'shared/ui/prefix';

export const bem = uiBem('{{component-name}}');
