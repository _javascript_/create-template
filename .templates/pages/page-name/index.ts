import React from "react";
import { PAGE_NAMES, PageConfig } from "shared/config/pages";

const Page = React.lazy(() => import("./ui"));

export const {{PAGE_NAME}}: PageConfig = {
  pageName: PAGE_NAMES.{{PAGE_NAME}},
  component: Page,
};
