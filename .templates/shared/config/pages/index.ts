*@*
{
    "type": "insert-import-to-end",
    "content": "import { {{pageName}} } from \"./{{page-name}}\";",
    "defaultContent": ["import { PagesMetaData } from \"./_types\";"]
}
*@*

export * from "./_types";
export * from "./_page-names";

*@*
{
    "type": "insert-to-end",
    "start": "export const PAGES_META_DATA: PagesMetaData = {",
    "content": "[{{pageName}}.pageName]: {{pageName}}.metadata",
    "end": "};"
}
*@*

*@*
{
    "type": "insert-to-end",
    "start": "export const REDIRECTS_PAGES = {",
    "content": "...{{pageName}}.redirects",
    "end": "};"
}
*@*

*@*
{
    "type": "insert-to-end",
    "start": "export const paths = {",
    "content": "[{{pageName}}.pageName]: {{pageName}}.path",
    "end": "};"
}
*@*

export type Paths = typeof paths;
export type Path = Paths[keyof Paths];
