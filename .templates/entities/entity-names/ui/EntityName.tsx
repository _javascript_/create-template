import React from 'react';

import { clsx } from 'shared/lib/clsx'

import { bem } from '../utils'

import "./{{EntityName}}.css"

namespace {{EntityName}}Row {
  export type Props = {
    className: string;
    children: React.ReactNode;
  }
}

function {{EntityName}}Row(props: {{EntityName}}.Props) {
  const { className, children } = props;
  const classes = clsx(bem('row'), className);

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

namespace {{EntityName}}Card {
  export type Props = {
    className: string;
    children: React.ReactNode;
  }
}

function {{EntityName}}Card(props: {{EntityName}}.Props) {
  const { className, children } = props;
  const classes = clsx(bem('card'), className);

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

export { {{EntityName}}Row, {{EntityName}}Card };