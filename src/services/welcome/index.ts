import { WelcomeOptions } from '../../types';
// import { sleep } from '../../utils/sleep';

export async function welcome(options?: WelcomeOptions) {
  console.clear();
  if (options) {
    const { description, title } = options;
    if (title) {
      console.log(title);
      // await sleep();
    }
    description && console.log(description);
  }
}
