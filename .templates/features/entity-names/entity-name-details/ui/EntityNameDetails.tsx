import React from 'react';

import { clsx } from 'shared/lib/clsx';

import { bem } from '../utils';

import './{{EntityName}}Details.css';


namespace {{EntityName}}Details {
  export type Props = {
    className: string;
    children: React.ReactNode;
  }
}

function {{EntityName}}Details(props: {{EntityName}}Details.Props) {
  const { className, children } = props;
  const classes = clsx(bem(), className);

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

export { {{EntityName}}Details };
