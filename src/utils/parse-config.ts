import fs from 'fs';

import { ENCODING } from '../constants';

export async function parseConfig(pathToConfig: string) {
  const data = fs.readFileSync(pathToConfig, ENCODING.UTF8);
  try {
    const result = JSON.parse(data);
    return result;
  } catch (error: any) {
    throw new Error('[utils]: parseConfig -> ' + error.toString());
  }
}
