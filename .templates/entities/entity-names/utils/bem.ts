import { bem as bemLib } from 'shared/lib/bem';

export const bem = bemLib(`{{entity-name}}`);
