import { Config, welcome, Questions, Template } from './services';

async function bootstrap() {
  const config = await Config.getInstance();

  await welcome(config.welcome);

  const questions = await Questions.init(config);

  const template = await Template.init(config, questions);

  template.start();
}

bootstrap();
