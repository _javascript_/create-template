export function sleep(ms = 3000) {
  return new Promise<void>((resolve) => {
    const id = setTimeout(() => {
      clearTimeout(id);
      resolve();
    }, ms);
  });
}
