"questions": {
"pages": [
{
"key": "page-name",
"message": "Название страницы?",
"type": "input"
},
{
"key": "page-path",
"message": "Url страницы?",
"type": "input"
}
],
"api": [
{
"key": "api-name",
"message": "Название api?",
"type": "input"
},
{
"key": "api-entity-name",
"message": "Название сущности?",
"type": "input"
},
{
"key": "api-controller-path",
"message": "Url контроллера?",
"type": "input"
}
],
"entities": [
{
"key": "entity-name",
"message": "Название сущности?",
"type": "input"
}
],
"ui": [
{
"key": "component-name",
"message": "Название компонента?",
"type": "input"
}
]
}
