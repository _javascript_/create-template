import React from 'react';

import { clsx } from 'shared/lib/clsx';

import { bem } from '../utils';

import './{{EntityName}}PagedList.css';


namespace {{EntityName}}PagedList {
  export type Props = {
    className: string;
    children: React.ReactNode;
  }
}

function {{EntityName}}PagedList(props: {{EntityName}}PagedList.Props) {
  const { className, children } = props;
  const classes = clsx(bem(), className);

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

export { {{EntityName}}PagedList };
