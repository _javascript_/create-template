import React from 'react';

import { clsx } from 'shared/lib/clsx';

import { bem } from '../utils';

import './{{EntityName}}Create.css';


namespace {{EntityName}}Create {
  export type Props = {
    className: string;
    children: React.ReactNode;
  }
}

function {{EntityName}}Create(props: {{EntityName}}Create.Props) {
  const { className, children } = props;
  const classes = clsx(bem(), className);

  return (
    <div className={classes}>
      {children}
    </div>
  )
}

export { {{EntityName}}Create };
