import React from "react";

import { router } from "shared/lib";
import { Page } from "shared/ui/atoms";

import { usePage } from "pages/utils";

import { PageTitle } from "./components";

function {{PageName}}(props: router.InjectionProps) {
  const { helmet, title } = usePage(props);

  return (
    <Page helmet={helmet}>
      <PageTitle title={title} />
    </Page>
  );
}

export default {{PageName}};
